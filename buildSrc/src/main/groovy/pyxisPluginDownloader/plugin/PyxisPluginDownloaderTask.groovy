package com.pyxis.hybris.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import com.github.skhatri.s3aws.client.S3Client

class PyxisPluginDownloadTask extends DefaultTask {

    @TaskAction
    def perform() {
        logger.quiet 'Starting Pyxis plugin parsing...'
        def plugins = parsePlugins(project)
        if(!plugins.isEmpty())
        {
        	logger.quiet "Found ${plugins.size()} plugin/s to process"
        	plugins.each { plugin ->
        		if(plugin.enabled){
        			logger.quiet "Processing plugin '${plugin.name}' with version '${plugin.version}'"

        			// DOWNLOAD
        			String pluginFolder = "temp/extensions/${plugin.name}"
        			(new File(pluginFolder)).mkdirs()
        			String saveTo = "${pluginFolder}/${plugin.name}-${plugin.version}.zip"

        			if(!(new File(saveTo)).exists())
        			{
        				def s3Ext = project.extensions.getByName('s3')
        				S3Client client = new S3Client(s3Ext.accessKey, s3Ext.secretKey, null)
        				client.downloadFile(s3Ext.bucket, "extensions/${plugin.name}/${plugin.name}-${plugin.version}.zip", saveTo)
        			}
        			else
        			{
        				logger.quiet "Plugin ${plugin.name}-${plugin.version} already downloaded"
        			}

        			// EXTRACT
        			if(!(new File("hybris/bin/custom/${plugin.name}").exists()))
        			{
        				def ant = new AntBuilder()
        				ant.unzip(src: saveTo, dest: "hybris/bin/custom/", overwrite: false){
					    	patternset( ){
					    		include( name: "${plugin.name}/**")
					    	}
					    }
						logger.quiet "!! IMPORTANT !! Remember to add <extension name=\"${plugin.name}\" /> to your config/commons/localextensions.xml"
        			}
        			else
        			{
        				logger.quiet "Will not process plugin ${plugin.name} as is already present under /custom folder"
        			}
        		}else{
        			logger.quiet "Plugin '${plugin.name} is disabled, will do nothing."
        		}
        	}

        }
        else
        {
        	logger.quiet "No plugins configured."
        }
    }


    def parsePlugins(project)
    {
    	 def plugins = []
        project.properties.each {prop ->
          if(prop.key.startsWith("plugins.")){
              def pluginArray = prop.key.split("\\.")
              def existingPlugin = plugins.find {it.name.equals(pluginArray[1])}
              if(existingPlugin == null) {
              	existingPlugin = new PyxisPlugin()
								existingPlugin.name = pluginArray[1]
              	plugins.add(existingPlugin);
              }
              existingPlugin.setAttribute(pluginArray[2], prop.value)
          }
      	}
      	return plugins;
    }


    class PyxisPlugin {
    	String name
    	Boolean enabled
    	String version

    	public void setAttribute(String name, String value)
    	{
    		if(name.equals("enabled")){
    			this.enabled = Boolean.valueOf(value)
    		}else if(name.equals("version")){
    			this.version = value
    		}
    	}

    	public boolean equals(Object obj)
    	{
    		return this.name.equals( ((PyxisPlugin)obj).name);
    	}

    	public setName(String name)
    	{
    		this.name = name;
    	}

    	public String toString()
    	{
    		return this.name + ": enabled=" + this.enabled +  " version=" + this.version
    	}

    }

}