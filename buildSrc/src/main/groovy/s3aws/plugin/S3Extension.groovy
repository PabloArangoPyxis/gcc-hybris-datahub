package com.github.skhatri.s3aws.plugin

class S3Extension {
    String bucket
    String accessKey = 'accessKey'
    String secretKey = 'secretKey'
    S3DownloadExtension download = new S3DownloadExtension()
    S3UploadExtension upload = new S3UploadExtension()
}
