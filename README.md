##GCC PCM

GCC PCM was developed using gradle build tool and Pyxis Hybris Template. It allows for an easy way to setup your Hybris project as well as making it easier to manage jenkins builds and deployments.

Please check the full documentation at: [https://pyxisjira.atlassian.net/wiki/display/ECOM/Pyxis+hybris+template](https://pyxisjira.atlassian.net/wiki/display/ECOM/Pyxis+hybris+template)

### SETUP 

**tools to use**
Mysql
Intellij
Git
Jenkins

1.
-Download and install Mysql
-Download and install intellij
-Request access too the wiki
-Clone project using git

2.
 **Create database "gcc" using Mysql**
 a) Run the following script in the MYSQL console
 b) create database gcc CHARACTER SET utf8 COLLATE utf8_bin;
 c) create user gcc@‘localhost' identified by 'Grupocorporacioncontrol123';   
 d) grant all privileges on gcc.* to 'gcc'@'localhost';
 
 note:
 "script to create the databases gcc"
 "create gcc user in Mysql"
 "all permissions are assigned to the created user gcc"
 
3. 
**Lift hybris server**
 a) In the project root downloaded from git, run terminal the following code "./gradlew setup"
 b) In the project root downloaded from git, go to "hybris/bin/platform" run terminal the following code ". ./setantenv.sh"
 c) In the project root dowmloaded from git, run terminal the following code "./gradlew"
 d) In the project root downloades from git, run terminal the following code "./gradlew initialize"
 e) In the project root downloaded from git, go to "hybris/bin/platform" run terminal the following code "./hybrisserver.sh"
