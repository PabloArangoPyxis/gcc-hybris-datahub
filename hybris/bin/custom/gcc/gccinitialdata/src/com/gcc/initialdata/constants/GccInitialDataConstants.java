/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.gcc.initialdata.constants;

/**
 * Global class for all GccInitialData constants.
 */
public final class GccInitialDataConstants extends GeneratedGccInitialDataConstants
{
	public static final String EXTENSIONNAME = "gccinitialdata";

	private GccInitialDataConstants()
	{
		//empty
	}
}
